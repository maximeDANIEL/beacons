package com.immersion.beacons;


import java.util.HashMap;

/**
 * Beacon class
 * Created by Maxime on 12/02/2015.
 */
public class Beacon extends HashMap<Integer, Data> {
    // beacon name
    private String name;
    // beacon adress
    private String MACAdress;

    /**
     * Default constructor
     */
    public Beacon(){
        super();
        this.setName("");
        this.setMACAdress("");
    }

    /**
     * Beacon Constructor with name and adress
     * @param name
     * @param MACAdress
     */
    public Beacon(String name,String MACAdress){
        super();
        this.setName(name);
        this.setMACAdress(MACAdress);

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMACAdress() {
        return MACAdress;
    }

    public void setMACAdress(String MACAdress) {
        this.MACAdress = MACAdress;
    }
}
