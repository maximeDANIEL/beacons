package com.immersion.beacons;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Beacon file Manager Class
 * Created by Maxime on 18/02/2015.
 */
public class BeaconFileManager {
    // current context
    private Context context;

    /**
     * Default constructor with context
     * @param context
     */
    public BeaconFileManager(Context context){
        this.setContext(context);
    }
    /**
     * load a beacon from file
     * @param filename
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public Beacon loadBeacon(String filename){
            Data.NEW_ID=0;
            Beacon res = new Beacon();
            try(FileInputStream input = context.openFileInput(filename)) {
                Scanner sc = new Scanner(input);
                String line;
                while (sc.hasNextLine()) {
                    line = sc.nextLine();
                    if(line.equals(MainActivity.nameField)){
                        line = sc.nextLine();
                        res.setName(line);
                    }
                    if(line.equals(MainActivity.MACAdressField)){
                        line = sc.nextLine();
                        res.setMACAdress(line);
                    }
                    if(line.equals(MainActivity.videoField)){
                        String path = sc.nextLine();
                        Data newData = new Data(Data.VIDEO_TYPE,path);
                        res.put(newData.getID(),newData);
                    }
                    if(line.equals(MainActivity.imageField)){
                        String path = sc.nextLine();
                        Data newData = new Data(Data.IMAGE_TYPE,path);
                        res.put(newData.getID(),newData);
                    }
                    if(line.equals(MainActivity.pdfField)){
                        String path = sc.nextLine();
                        Data newData = new Data(Data.PDF_TYPE,path);
                        res.put(newData.getID(),newData);
                    }
                }
                input.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
       return res;
     }

    /**
     * save beacon into file
     * @param filename
     */
    public void saveBeacon(String filename,Beacon beacon){
        FileOutputStream output;
        try {
            output = context.openFileOutput(filename, Context.MODE_PRIVATE);
            String name = MainActivity.nameField+"\n"+beacon.getName()+"\n";
            String MACAdress = MainActivity.MACAdressField+"\n"+beacon.getMACAdress()+"\n";
            ArrayList<String> dataList1 = new ArrayList<>();
            for(Data data : beacon.values()){
                switch(data.getType()){
                    case Data.VIDEO_TYPE :
                        dataList1.add(MainActivity.videoField+"\n"+data.getPath()+"\n");
                        break;
                    case Data.IMAGE_TYPE :
                        dataList1.add(MainActivity.imageField+"\n"+data.getPath()+"\n");
                        break;
                    case Data.PDF_TYPE :
                        dataList1.add(MainActivity.pdfField+"\n"+data.getPath()+"\n");
                        break;
                }
            }
            try {
                output.write(name.getBytes());
                output.write(MACAdress.getBytes());

                for(String data : dataList1){
                    output.write(data.getBytes());
                }

            } catch (IOException f) {
                f.printStackTrace();
            }
            output.close();
        } catch (FileNotFoundException f) {
            f.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
