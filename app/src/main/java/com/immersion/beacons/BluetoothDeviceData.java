package com.immersion.beacons;

import java.util.ArrayList;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**
 * BluetoothDevice class
 */
public class BluetoothDeviceData implements Cloneable, Parcelable,  Comparable<BluetoothDeviceData>{
    // beacon address
	private String macAddress;
    // beacon rssi;
	private int rssi;
    // array of last beacon rssi
    ArrayList<Integer> listRssi;
	
	static public final Parcelable.Creator<BluetoothDeviceData> CREATOR = new Parcelable.Creator<BluetoothDeviceData>() {

		public BluetoothDeviceData createFromParcel(Parcel in) {
			return new BluetoothDeviceData(in);
		}

		public BluetoothDeviceData[] newArray(int size) {
			return new BluetoothDeviceData[size];
		}
	};

    /**
     * BluetoothDevice constructor with a macAdress and the first rssi
     * @param macAddress
     * @param rssi
     */
	public BluetoothDeviceData(String macAddress, int rssi)
	{
		this.setMacAddress(macAddress);
		this.rssi  = rssi;
        listRssi = new ArrayList<>();
        listRssi.add(rssi);
	}

	public int getRssi() {
		return rssi;
	}


	public void addRssi(int pRssi)
	{
        listRssi.add(pRssi);

	}
 	public void setRssi(int rssi) {
    this.rssi  = rssi;
    listRssi.clear();
    listRssi.add(rssi);
    }
    public void printListRssi()
    {
        Log.i("BluetoothDeviceData : ",listRssi.toString());
    }
    public void printRssi()
    {
        Log.i("BluetoothDeviceData : ",Float.toString(rssi));
    }

    /**
     * Interpolate rssi array
     */
    public void interpolate()
    {
        double mean = mean(listRssi);
        double std = std(mean,listRssi);
        ArrayList<Integer> filterListRssi= new ArrayList<>();
        for(int i : listRssi){
            if(i>=-3*std+mean && i<=3*std+mean)filterListRssi.add(i);
        }
        if(listRssi.size()>MainActivity.rssiSettings.getSampleSize()){
            int nbToErase=listRssi.size()-MainActivity.rssiSettings.getSampleSize();
            for(int i =0;i<nbToErase;i++)
                listRssi.remove(0);
        }
        rssi= (int)Math.round(mean(filterListRssi));

    }

    /**
     *  compute list mean
     * @param list
     * @return
     */
    public double mean(ArrayList<Integer> list){
        double mean=0;
        for(int i : list){
            mean+=i;
        }
        return mean/list.size();
    }

    /**
     *  compute list standard deviation
     * @param mean
     * @param list
     * @return
     */
    public double std(double mean,ArrayList<Integer> list){
        double std=0;
        for(int i : list){
            std+=Math.pow(i - mean, 2);
        }
        return Math.sqrt(std);
    }
    /**
     * Compare BluetoothDeviceData
     * @param another
     * @return
     */
    @Override
    public int compareTo(BluetoothDeviceData another) {
        return getRssi() > another.getRssi() ? -1 : 1;
    }

	public BluetoothDeviceData(Parcel in) {
		this.setMacAddress(in.readString());
		this.rssi = in.readInt();
        listRssi = new ArrayList<>();
        listRssi.add(rssi);

	}

	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(macAddress); 
		dest.writeInt(rssi);
        listRssi = new ArrayList<>();
        listRssi.add(rssi);
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

}
