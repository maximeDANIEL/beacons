package com.immersion.beacons;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

/**
 * BluetoothManagement class
 */
public class BluetoothManagement extends BroadcastReceiver {
    // beacons data
	public HashMap<String, BluetoothDeviceData> btDeviceMap = new HashMap<String, BluetoothDeviceData>();
    // beacons life
    public HashMap<String, Integer> btDeviceMapLife = new HashMap<String, Integer>();
    // temporary beacon list
    public ArrayList<String> tmpBtDeviceMap = new ArrayList<String>();
    // observer list
	private ArrayList<IBluetoothUpdateObservers> observers = new ArrayList<IBluetoothUpdateObservers>();

    /**
     * Register observer
     * @param observer
     */
	public void registerObserver(IBluetoothUpdateObservers observer)
	{
		observers.add(observer);
	}

    /**
     * Manage new incoming intent
     * @param context
     * @param intent
     */
	@Override
	public void onReceive(Context context, Intent intent) {
		
		Bundle bundle = intent.getExtras();
            if(bundle==null){
                String[] keys=btDeviceMap.keySet().toArray(new  String[btDeviceMap.keySet().size()]);
                for(int i=0;i<keys.length;i++){
                    if(!tmpBtDeviceMap.contains(keys[i])){
                        if(!btDeviceMapLife.containsKey(keys[i]))btDeviceMapLife.put(keys[i],0);
                        int life=btDeviceMapLife.get(keys[i])+1;
                        if(life>MainActivity.rssiSettings.getSampleSize()) {

                            btDeviceMapLife.remove(keys[i]);
                            btDeviceMap.remove(keys[i]);
                        }
                        else btDeviceMapLife.put(keys[i],life);

                    }
                }
                tmpBtDeviceMap.clear();
                for(IBluetoothUpdateObservers obs : observers)
                {
                    obs.bluetoothListUpdate(btDeviceMap);
                }
                return;
            }
		BluetoothDeviceData data = (BluetoothDeviceData)bundle.get(MainActivity.MACAdressField);
        BluetoothDeviceData device = btDeviceMap.get(data.getMacAddress());
        if (device != null) {
            if(!tmpBtDeviceMap.contains(data.getMacAddress())){
                tmpBtDeviceMap.add(data.getMacAddress());
                btDeviceMapLife.remove(data.getMacAddress());
            }

             device.addRssi(data.getRssi());
            device.interpolate();
        }
        else {
            btDeviceMap.put(data.getMacAddress(), data);
            tmpBtDeviceMap.add(data.getMacAddress());
            btDeviceMapLife.remove(data.getMacAddress());
        }
	}

	public HashMap<String, BluetoothDeviceData> getBtDeviceMap() {
		return btDeviceMap;
	}

	public void setBtDeviceMap(HashMap<String, BluetoothDeviceData> btDeviceMap) {
		this.btDeviceMap = btDeviceMap;
	}
}
