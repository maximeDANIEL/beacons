package com.immersion.beacons;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * ContentSettingActivity class
 */
public class ContentSettingActivity extends ActionBarActivity{
    public static ContentSettingActivity contentSettingActivity;
    // data adapter list
    private ArrayAdapter<Data> mAdapter;
    // view list
    private ListView contentListView;
    // data list
    private ArrayList<Data> dataList;
    // add beacon button
    private Button addButton;
    // current beacon
    private Beacon beacon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentSettingActivity = this;
        setContentView(R.layout.activity_content_setting);
        beacon = MainActivity.beaconMap.get(getIntent().getStringExtra(MainActivity.MACAdressField));
        contentListView = (ListView) findViewById(R.id.contentList);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffec6200")));
        getSupportActionBar().setTitle(beacon.getName());
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        dataList = new ArrayList<>();
        dataList.addAll(beacon.values());

        mAdapter = new ArrayAdapter<Data>(this,R.layout.row_layout_content,dataList) {
            public View getView(final int position, View convertView, ViewGroup parent) {
                if (convertView == null) {
                    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.row_layout_content, parent, false);
                }
                final Data current = dataList.get(position);
                ImageView imageView = (ImageView)convertView.findViewById(R.id.imageView);
                switch(current.getType()){
                    case Data.VIDEO_TYPE :
                        imageView.setImageResource(R.drawable.ic_video_file);
                        break;
                    case Data.IMAGE_TYPE :
                        imageView.setImageResource(R.drawable.ic_image_file);
                        break;
                    case Data.PDF_TYPE :
                        imageView.setImageResource(R.drawable.ic_pdf_file);
                        break;
                }
                TextView text1 = (TextView) convertView.findViewById(R.id.textView1);
                text1.setText(Data.getStringOf(current.getType()));
                TextView text2 = (TextView) convertView.findViewById(R.id.textView2);
                text2.setText(current.getPath());
                convertView.findViewById(R.id.buttonLayout);
                View buttonLayout = convertView.findViewById(R.id.buttonLayout);

                Button modifyButton = (Button)buttonLayout.findViewById(R.id.modifyButton);
                modifyButton.setOnClickListener(new View.OnClickListener(){
                    @Override
                        public void onClick(View v) {

                            Intent intent = new Intent(ContentSettingActivity.this, ListFileActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.putExtra(MainActivity.MACAdressField, beacon.getMACAdress());
                            intent.putExtra(MainActivity.IDField, current.getID());
                            startActivity(intent);
                        }
                });
                Button deleteButton = (Button) buttonLayout.findViewById(R.id.deleteButton);
                deleteButton.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        beacon.remove(current.getID());
                        MainActivity.bfm.saveBeacon(beacon.getName(),beacon);
                        mAdapter.remove(current);
                        mAdapter.notifyDataSetChanged();
                    }
                });
                return convertView;
            }
        };
        addButton = (Button) findViewById(R.id.addButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                                Intent intent = new Intent(ContentSettingActivity.this, ListFileActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                intent.putExtra(MainActivity.MACAdressField, beacon.getMACAdress());
                                intent.putExtra(MainActivity.IDField, -1);
                                startActivity(intent);
            }
        });

        contentListView.setAdapter(mAdapter);
    }

    @Override
    protected void onResume(){
        super.onResume();
        beacon = MainActivity.bfm.loadBeacon(beacon.getName());
        mAdapter.clear();
        mAdapter.addAll(beacon.values());
        mAdapter.notifyDataSetChanged();

    }
}