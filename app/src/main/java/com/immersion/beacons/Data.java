package com.immersion.beacons;

/**
 * Data class
 * specify file type, file format and file path
 * Created by Maxime on 16/02/2015.
 */
public class Data {
    public static final int VIDEO_TYPE = 0;
    public static final int IMAGE_TYPE = 1;
    public static final int PDF_TYPE = 2;
    public static final String JPEG = "jpeg";
    public static final String GIF = "jpg";
    public static final String PNG = "png";
    public static final String BMP = "bmp";
    public static final String WEBP = "webp";
    public static final String GP3= "3gp";
    public static final String MP4 = "mp4";
    public static final String MKV = "mkv";
    public static final String WEBM = "webm";
    public static final String PDF = "pdf";
    public static int NEW_ID = 0;
    private int type;
    private int ID;
    private String path;
    public Data(int type, String path){
        ID=NEW_ID++;
        this.type=type;
        this.path=path;
    }
    public int getType() {
        return type;
    }
    /**
    public String getTypeString() {
        String res ="none";
        switch (type){
            case VIDEO_TYPE :
               res="video";
               break;
            case IMAGE_TYPE :
                res="image";
                break;
            case PDF_TYPE :
                res="pdf";
                break;
        }
        return res;
    }
    **/
    public void setType(int type) {
        this.type = type;
    }


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    /**
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
     **/
    public static String getStringOf(int type){
        String res ="none";
        switch (type){
            case VIDEO_TYPE :
                res="video";
                break;
            case IMAGE_TYPE :
                res="image";
                break;
            case PDF_TYPE :
                res="pdf";
                break;
        }
        return res;
    }
    /**
    public static int getIntOf(String s) {
        int res =-1;
        if(s.equals("video"))res=0;
        if(s.equals("image"))res=1;
        if(s.equals("pdf"))res=2;
        return res;
    }
    **/
    public int getID() {
        return ID;
    }
    /**
    public void setID(int ID) {
        this.ID = ID;
    }
    **/
    public static boolean isVideo(String extension) {
        return(extension.equals(GP3) || extension.equals(MP4) || extension.equals(MKV) || extension.equals(WEBM));
    }

    public static boolean isImage(String extension) {
        return(extension.equals(JPEG) || extension.equals(GIF) || extension.equals(PNG) || extension.equals(BMP) ||extension.equals(WEBP));
    }
    public static boolean isPdf(String extension) {
        return(extension.equals(PDF));
    }
}
