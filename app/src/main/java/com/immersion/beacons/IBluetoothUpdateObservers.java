package com.immersion.beacons;

import java.util.HashMap;

public interface IBluetoothUpdateObservers {
	void bluetoothListUpdate(HashMap<String, BluetoothDeviceData> devices);
}