package com.immersion.beacons;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;
import java.io.File;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * ImageActivity class
 */
public class ImageActivity extends Activity {
    public static ImageActivity imageActivity=null;
    // image file path
    private String path;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageActivity=this;
        setContentView(R.layout.activity_image);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ImageView imgView = (ImageView) findViewById(
                R.id.imageView);
        path=getIntent().getStringExtra(MainActivity.imageField);
        File file = new  File(path);
        if(file.exists())
        {
           imgView.setImageURI(Uri.fromFile(file));
        }
        new PhotoViewAttacher(imgView);
    }
}
