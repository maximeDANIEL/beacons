package com.immersion.beacons;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.animation.*;
import android.widget.ImageView;

/**
 * InitActivity class
 * Immersion Animation screen
 */
public class InitActivity extends ActionBarActivity {
    public static final int OFFSET_ANIM = 500;
    public static final int TIME_ANIM = 1000;
    private ImageView logoImmersion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        logoImmersion=(ImageView)findViewById(R.id.imageView);
        logoImmersion.setImageBitmap(BitmapFactory.decodeResource(getApplicationContext().getResources(),
                R.drawable.ic_launcher));
        AlphaAnimation animation1 = new AlphaAnimation(0.0f, 1.0f);
        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation arg0) {
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                startActivity(new Intent(InitActivity.this, MainActivity.class));
                finish();
            }
        });
        animation1.setDuration(TIME_ANIM);
        animation1.setStartOffset(OFFSET_ANIM);
        animation1.setFillAfter(true);
        logoImmersion.startAnimation(animation1);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_init, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
