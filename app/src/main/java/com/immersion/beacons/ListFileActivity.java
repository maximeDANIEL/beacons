package com.immersion.beacons;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class ListFileActivity extends ActionBarActivity implements AdapterView.OnItemClickListener{
    private String path;
    private ListView fileListView;
    private ArrayAdapter adapter;
    private Beacon beacon;
    private int ID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_file);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffec6200")));
        getSupportActionBar().setTitle("Selection");
        String MACAdress = getIntent().getStringExtra(MainActivity.MACAdressField);
        beacon = MainActivity.beaconMap.get(MACAdress);
        ID = getIntent().getIntExtra(MainActivity.IDField,-1);

        path = getIntent().getStringExtra(MainActivity.pathField);
        if(path==null || path.equals(""))path="/";
            fileListView = (ListView) findViewById(R.id.contentList);
            List values = new ArrayList<>();
            File dir = new File(path);
            if (!dir.canRead()) {
                setTitle(getTitle() + " (inacessible)");
            }
            String[] list = dir.list();
            if (list != null) {
                for (String file : list) {
                    if (!file.startsWith(".")) {
                        values.add(file);
                    }
                }
            }
            Collections.sort(values);

            adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_2, android.R.id.text1, values);
            fileListView.setAdapter(adapter);
            fileListView.setOnItemClickListener(this);
        }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String filename = (String) adapter.getItem(position);
        if (path.endsWith(File.separator)) {
            filename = path + filename;
        } else {
            filename = path + File.separator + filename;
        }
        if (new File(filename).isDirectory()) {
            Intent intent = new Intent(this, ListFileActivity.class);
            intent.putExtra(MainActivity.pathField, filename);
            intent.putExtra(MainActivity.MACAdressField, beacon.getMACAdress());
            intent.putExtra(MainActivity.IDField,ID);
            startActivity(intent);
        } else {
            Data current = beacon.get(ID);
            if (current == null) {
                int startIndex = filename.lastIndexOf('.');
                String extension = filename.substring(startIndex + 1, filename.length());
                if (Data.isVideo(extension)) {
                    current = new Data(Data.VIDEO_TYPE, filename);
                    beacon.put(current.getID(), current);
                    MainActivity.bfm.saveBeacon(beacon.getName(), beacon);
                    Toast.makeText(this, filename + " selected", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(this, ContentSettingActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    finish();
                } else if (Data.isImage(extension)) {
                    current = new Data(Data.IMAGE_TYPE, filename);
                    beacon.put(current.getID(), current);
                    MainActivity.bfm.saveBeacon(beacon.getName(), beacon);
                    Toast.makeText(this, filename + " selected", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(this, ContentSettingActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    finish();
                }else if (Data.isPdf(extension)) {
                    current = new Data(Data.PDF_TYPE, filename);
                    beacon.put(current.getID(), current);
                    MainActivity.bfm.saveBeacon(beacon.getName(), beacon);
                    Toast.makeText(this, filename + " selected", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(this, ContentSettingActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(this, filename + " is not an accepted format", Toast.LENGTH_LONG).show();
                }
            } else {
                int startIndex = filename.lastIndexOf('.');
                String extension = filename.substring(startIndex + 1, filename.length());
                if (Data.isVideo(extension)) {
                    current.setPath(filename);
                    current.setType(Data.VIDEO_TYPE);
                    beacon.put(current.getID(), current);
                    MainActivity.bfm.saveBeacon(beacon.getName(), beacon);
                    Toast.makeText(this, filename + " selected", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(this, ContentSettingActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    finish();
                } else if (Data.isImage(extension)) {
                    current.setPath(filename);
                    current.setType(Data.IMAGE_TYPE);
                    beacon.put(current.getID(), current);
                    MainActivity.bfm.saveBeacon(beacon.getName(), beacon);
                    Toast.makeText(this, filename + " selected", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(this, ContentSettingActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    finish();
                }else if (Data.isPdf(extension)) {
                    current.setPath(filename);
                    current.setType(Data.PDF_TYPE);
                    beacon.put(current.getID(), current);
                    MainActivity.bfm.saveBeacon(beacon.getName(), beacon);
                    Toast.makeText(this, filename + " selected", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(this, ContentSettingActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(this, filename + " is not an accepted format", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
