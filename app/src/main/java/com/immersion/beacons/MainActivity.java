package com.immersion.beacons;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.HashMap;

/**
 * MainActivity class
 * main activity
 */
public class MainActivity extends ActionBarActivity implements IBluetoothUpdateObservers, AdapterView.OnItemClickListener {
    public static MainActivity mainActivity;
    public static final String nameField = "name:";
    public static final String MACAdressField = "MACAdress:";
    public static final String IDField = "ID:";
    public static final String pathField = "path:";
    public static final String videoField = "video:";
    public static final String imageField = "image:";
    public static final String pdfField="pdf:";
    public static boolean isSearchingForBeacons=true;
    private static final long TIME_ANIM = 1000;
    public static HashMap<String, Beacon> beaconMap;
    public static BeaconFileManager bfm;
    public static SettingsFileManager sfm;
    public static AlphaAnimation animation1;
    public static AlphaAnimation animation2;
    public static ArrayList<BluetoothDeviceData> btDevices;
    public static RssiSettings rssiSettings;
    public static final String rssiSettingsFile = "RssiSettings";
    private ImageView NotifyImmersion;
    private Button switchButton;
    private Intent intentScan;
    private BluetoothManagement btManagement;
    private LinearLayout waitingLinearLayout;
    private ArrayAdapter<Beacon> mAdapter;
    private ArrayAdapter<BluetoothDeviceData> mVisibleBeaconAdapter;
    private ListView beaconListView;
    private ListView visibleBeaconListView;
    private ArrayList<BluetoothDeviceData> visibleBeaconsList;

    /**
     * get all immersion beacons
     */
    private void getBeaconMap(){
        beaconMap = new HashMap<>();
        beaconMap.put("F4:F4:0B:B1:49:02",getBeacon("Beacon1","Beacon1","F4:F4:0B:B1:49:02"));
        beaconMap.put("D5:E1:D2:74:03:02",getBeacon("Beacon2","Beacon2","D5:E1:D2:74:03:02"));
        beaconMap.put("E3:05:09:0E:E9:02",getBeacon("Beacon3","Beacon3","E3:05:09:0E:E9:02"));
        beaconMap.put("C1:FA:58:80:A4:02",getBeacon("Beacon4","Beacon4","C1:FA:58:80:A4:02"));
        beaconMap.put("F5:E2:48:77:E3:02",getBeacon("Beacon5","Beacon5","F5:E2:48:77:E3:02"));
        beaconMap.put("C2:A8:93:F0:6E:02",getBeacon("Beacon6","Beacon6","C2:A8:93:F0:6E:02"));
    }

    /**
     * get rssi from settings file
     */
    private void getRssiSettings(){
        rssiSettings = getRssiSettings(rssiSettingsFile);
    }

    /**
     * check bluetooth state
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK){
            Toast.makeText(getApplicationContext(), "Bluetooth turned on"
                    , Toast.LENGTH_LONG).show();
        }
        else{
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), 0);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity=this;
        Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(turnOn, 0);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffec6200")));
        //getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        waitingLinearLayout=(LinearLayout)findViewById(R.id.waitingLinearLayout);
        NotifyImmersion=(ImageView)findViewById(R.id.imageView2);
        animation1 = new AlphaAnimation(0.0f, 1.0f);
        animation1.setDuration(TIME_ANIM);
        animation1.setFillAfter(true);
        animation2 = new AlphaAnimation(1.0f, 0.0f);
        animation2.setDuration(TIME_ANIM);
        animation2.setFillAfter(true);
        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation arg0) {
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                NotifyImmersion.startAnimation(animation2);
            }
        });
        animation2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation arg0) {
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                NotifyImmersion.startAnimation(animation1);
            }
        });
        bfm = new BeaconFileManager(this.getApplicationContext());
        sfm = new SettingsFileManager(this.getApplicationContext());
        getBeaconMap();
        getRssiSettings();
        btManagement = new BluetoothManagement();
        btManagement.registerObserver(this);
        registerReceiver(btManagement, new IntentFilter(ScanDevicesService.BROADCAST_ACTION));
        visibleBeaconListView = (ListView) findViewById(R.id.visibleBeaconList);
        beaconListView = (ListView) findViewById(R.id.beaconList);
        final List<Beacon> beaconsList = new ArrayList<>();
        beaconsList.addAll(MainActivity.beaconMap.values());
        mAdapter= new ArrayAdapter<Beacon>(this,R.layout.row_layout,beaconsList){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                if(convertView==null){
                    LayoutInflater inflater= (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.row_layout,parent,false);
                }
                Beacon currentBeacon = beaconsList.get(position);
                TextView text1 = (TextView) convertView.findViewById(R.id.textView1);
                TextView text2 = (TextView) convertView.findViewById(R.id.textView2);
                text1.setText(currentBeacon.getName());
                text2.setText(currentBeacon.getMACAdress());
                convertView.setEnabled(true);
                convertView.setAlpha(1.0f);
               if(currentBeacon.size()==0){
                   convertView.setEnabled(false);
                   convertView.setAlpha(0.5f);
               }
                return convertView;
            }
            @Override
            public boolean isEnabled(int position) {
                return  beaconsList.get(position).size()!=0;
            }
        };
        beaconListView.setAdapter(mAdapter);
        beaconListView.setOnItemClickListener(this);
        switchButton = (Button) findViewById(R.id.switchButton);
        switchButton.setOnClickListener(new Button.OnClickListener() {
                                            public void onClick(View v) {
                                                //Do stuff here
                                                if (!isSearchingForBeacons) {
                                                    isSearchingForBeacons=true;
                                                    if(ScanDevicesService.scanDevicesService!=null)ScanDevicesService.scanDevicesService.onResume();
                                                    beaconListView.setVisibility(View.INVISIBLE);
                                                    waitingLinearLayout.setVisibility(View.VISIBLE);
                                                    visibleBeaconListView.setVisibility(View.VISIBLE);
                                                    NotifyImmersion.setVisibility(View.VISIBLE);
                                                    NotifyImmersion.startAnimation(animation2);
                                                    switchButton.setText(getText(R.string.beacon_list));

                                                } else{
                                                    isSearchingForBeacons=false;
                                                    if(ScanDevicesService.scanDevicesService!=null)ScanDevicesService.scanDevicesService.onPause();
                                                    else {
                                                        intentScan = new Intent(mainActivity, ScanDevicesService.class);
                                                        startService(intentScan);
                                                    }
                                                    waitingLinearLayout.setVisibility(View.INVISIBLE);
                                                    NotifyImmersion.setVisibility(View.INVISIBLE);
                                                    visibleBeaconListView.setVisibility(View.INVISIBLE);
                                                    animation2.reset();
                                                    animation1.reset();
                                                    NotifyImmersion.clearAnimation();
                                                    beaconListView.setVisibility(View.VISIBLE);
                                                    switchButton.setText(getText(R.string.beacon_search));
                                                }
                                            }
                                        }
        );
        if(isSearchingForBeacons){
            isSearchingForBeacons=true;
            if(ScanDevicesService.scanDevicesService!=null)ScanDevicesService.scanDevicesService.onResume();
            else {
                intentScan = new Intent(this, ScanDevicesService.class);
                startService(intentScan);
            }
            beaconListView.setVisibility(View.INVISIBLE);
            waitingLinearLayout.setVisibility(View.VISIBLE);
            NotifyImmersion.setVisibility(View.VISIBLE);
            visibleBeaconListView.setVisibility(View.VISIBLE);
            NotifyImmersion.startAnimation(animation2);
            switchButton.setText(getText(R.string.beacon_list));
        }
        else {
            isSearchingForBeacons=false;
            if(ScanDevicesService.scanDevicesService!=null)ScanDevicesService.scanDevicesService.onPause();
            waitingLinearLayout.setVisibility(View.INVISIBLE);
            NotifyImmersion.setVisibility(View.INVISIBLE);
            visibleBeaconListView.setVisibility(View.INVISIBLE);
            animation2.reset();
            animation1.reset();
            NotifyImmersion.clearAnimation();
            beaconListView.setVisibility(View.VISIBLE);
            switchButton.setText(getText(R.string.beacon_search));
        }

        //animation1.setRepeatCount(Animation.INFINITE);


    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /**
     * get beacon from file
     * @param filename
     * @param beaconName
     * @param beaconMACAdress
     * @return
     */
    public Beacon getBeacon(String filename,String beaconName,String beaconMACAdress){
        Beacon res = bfm.loadBeacon(filename);
        if(res==null){
            res=new Beacon(beaconName,beaconMACAdress);
            bfm.saveBeacon(filename,res);
        }
        return res;
    }

    /**
     * get rssi settings
     * @param filename
     * @return
     */
    public RssiSettings getRssiSettings(String filename){
        RssiSettings res = sfm.loadRssiSettings(filename);
        if(res==null){
            res=new RssiSettings(getResources().getInteger(R.integer.rssi_min_default),getResources().getInteger(R.integer.rssi_max_default),getResources().getInteger(R.integer.sample_size_default));
            sfm.saveRssiSettings(filename,res);
        }
        return res;
    }

    /**
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * menu item handler
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(ScanDevicesService.scanDevicesService!=null)ScanDevicesService.scanDevicesService.onPause();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this, SettingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            return true;
        }
        if (id == R.id.rssi_settings) {
            Intent intent = new Intent(MainActivity.this, RssiSettingsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     *  update BluetoothDevice list
     * @param devices
     */
    @Override
    public void bluetoothListUpdate(HashMap<String, BluetoothDeviceData> devices) {
            btDevices = new ArrayList<>(devices.values());
            if (btDevices.size() > 0) {
                Collections.sort(btDevices);
                visibleBeaconsList = new ArrayList<>(btDevices);
                    mVisibleBeaconAdapter = new ArrayAdapter<BluetoothDeviceData>(this, R.layout.row_layout2, visibleBeaconsList) {
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            if (convertView == null) {
                                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                convertView = inflater.inflate(R.layout.row_layout2, parent, false);
                            }
                            BluetoothDeviceData currentBluetoothDeviceData = visibleBeaconsList.get(position);
                            Beacon currentBeacon = beaconMap.get(currentBluetoothDeviceData.getMacAddress());
                            TextView text1 = (TextView) convertView.findViewById(R.id.textView1);
                            TextView text2 = (TextView) convertView.findViewById(R.id.textView2);
                            TextView text3 = (TextView) convertView.findViewById(R.id.textView3);
                            SeekBar seekBar = (SeekBar) convertView.findViewById(R.id.rssiSeekBar);
                            TextView text4 = (TextView) convertView.findViewById(R.id.MaxRssiRangeTextView);
                            TextView text5 = (TextView) convertView.findViewById(R.id.MinRssiRangeTextView);
                            text1.setText(currentBeacon.getName());
                            DecimalFormat df = new DecimalFormat("0.00");
                            double percent=getPercentOf(currentBluetoothDeviceData.getRssi());
                            text2.setText(df.format(percent)+"%");
                            text3.setText(String.valueOf(currentBluetoothDeviceData.getRssi()));
                            seekBar.setProgress((int)percent);
                            text4.setText(getText(R.string.rssi_max_text)+"\n"+String.valueOf(rssiSettings.getMaxRssi()));

                            text5.setText(getText(R.string.rssi_min_text)+"\n"+String.valueOf(rssiSettings.getMinRssi()));
                            convertView.setEnabled(true);
                            convertView.setAlpha(1.0f);
                            if (currentBeacon.size() == 0) {
                                convertView.setEnabled(false);
                                convertView.setAlpha(0.5f);
                            }
                            return convertView;
                        }

                        @Override
                        public boolean isEnabled(int position) {
                            BluetoothDeviceData currentBluetoothDeviceData = visibleBeaconsList.get(position);
                            Beacon currentBeacon = beaconMap.get(currentBluetoothDeviceData.getMacAddress());
                            return currentBeacon.size() != 0;
                        }
                    };
                    visibleBeaconListView.setAdapter(mVisibleBeaconAdapter);
                    visibleBeaconListView.setVisibility(View.VISIBLE);

                for (int i = btDevices.size() - 1; i > -1; i--) {
                    if (btDevices.get(i).getRssi() < rssiSettings.getMinRssi()) {
                        btDevices.remove(i);
                    }
                }
            }
            if (btDevices.size() > 0) {
                BluetoothDeviceData btdevice = btDevices.get(0);
                String MACAdress = btdevice.getMacAddress();
                Beacon beacon = beaconMap.get(MACAdress);

                if (beacon != null) {
                    if (beacon.size() != 0) {

                        if (MosaicActivity.mosaicActivity == null || !MosaicActivity.mosaicActivity.beacon.getMACAdress().equals(MACAdress)) {

                            if(MosaicActivity.mosaicActivity != null) {
                                MosaicActivity.mosaicActivity.finish();
                                MosaicActivity.mosaicActivity=null;
                               }
                            Intent intent = new Intent(MainActivity.this, MosaicActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.putExtra(MACAdressField, beacon.getMACAdress());
                            startActivity(intent);

                        }
                    }

                }
            }
    }

    /**
     * when resuming activity
     */
    @Override
    public void onResume() {
        super.onResume();
        getBeaconMap();
        getRssiSettings();
        final List<Beacon> beaconsList = new ArrayList<>();
        beaconsList.addAll(MainActivity.beaconMap.values());
        mAdapter= new ArrayAdapter<Beacon>(this,R.layout.row_layout,beaconsList){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                if(convertView==null){
                    LayoutInflater inflater= (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.row_layout,parent,false);
                }
                Beacon currentBeacon = beaconsList.get(position);
                TextView text1 = (TextView) convertView.findViewById(R.id.textView1);
                TextView text2 = (TextView) convertView.findViewById(R.id.textView2);
                text1.setText(currentBeacon.getName());
                text2.setText(currentBeacon.getMACAdress());
                convertView.setEnabled(true);
                convertView.setAlpha(1.0f);
                if(currentBeacon.size()==0){
                    convertView.setEnabled(false);
                    convertView.setAlpha(0.5f);
                }
                return convertView;
            }
            @Override
            public boolean isEnabled(int position) {
                return  beaconsList.get(position).size()!=0;
            }
        };
        mAdapter.notifyDataSetChanged();
        //beaconListView.setAdapter(mAdapter);
        //beaconListView.setOnItemClickListener(this);
        if(isSearchingForBeacons) {
            if (ScanDevicesService.scanDevicesService != null)
                ScanDevicesService.scanDevicesService.onResume();
        }
    }

    /**
     * when finishing activity
     */
    @Override
    public void finish() {
        super.finish();
        isSearchingForBeacons=false;
        unregisterReceiver(btManagement);
    }

    /**
     * when destroying activity
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        if(ScanDevicesService.scanDevicesService!=null) {
            ScanDevicesService.scanDevicesService.onPause();
            ScanDevicesService.scanDevicesService.finish();
            ScanDevicesService.scanDevicesService.onDestroy();
        }
    }

    /**
     * get percent of rssi range
     * @param newRssi
     * @return
     */
    public static double getPercentOf(double newRssi){
        if(newRssi<rssiSettings.getMaxRssi())newRssi=rssiSettings.getMaxRssi();
        if(newRssi>rssiSettings.getMinRssi())newRssi=rssiSettings.getMinRssi();
        double ecartMax=rssiSettings.getMinRssi()-rssiSettings.getMaxRssi();
        double ecart= newRssi-rssiSettings.getMaxRssi();
        return (ecart/ecartMax)*100;
    }

    /**
     * when clicking on beacon item
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Beacon beacon =  mAdapter.getItem(position);
        view.setEnabled(false);
        if(beacon!=null) {
            if(beacon.size()!=0){
                if(MosaicActivity.mosaicActivity==null || !MosaicActivity.mosaicActivity.beacon.getMACAdress().equals(beacon.getMACAdress())){
                    if(MosaicActivity.mosaicActivity!=null){
                        MosaicActivity.mosaicActivity.finish();
                        MosaicActivity.mosaicActivity=null;
                    }
                    Intent intent = new Intent(MainActivity.this,MosaicActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra(MACAdressField,beacon.getMACAdress());
                    startActivity(intent);
                }
            }
            else  Toast.makeText(this,"Beacon : "+ beacon.getName() + " : " + beacon.getMACAdress()+" : "+" has no assigned file. \n", Toast.LENGTH_LONG).show();

        }
    }
}
