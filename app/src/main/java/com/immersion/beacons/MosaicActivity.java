package com.immersion.beacons;
        import java.io.File;
        import java.util.ArrayList;
        import java.util.HashMap;
        import java.util.List;

        import android.animation.Animator;
        import android.animation.AnimatorListenerAdapter;
        import android.animation.AnimatorSet;
        import android.animation.ObjectAnimator;
        import android.app.ProgressDialog;
        import android.content.Context;
        import android.content.Intent;
        import android.content.pm.ActivityInfo;
        import android.content.res.Configuration;
        import android.graphics.Bitmap;
        import android.graphics.BitmapFactory;
        import android.graphics.Color;
        import android.graphics.Point;
        import android.graphics.Rect;
        import android.graphics.drawable.ColorDrawable;
        import android.graphics.drawable.Drawable;
        import android.net.Uri;
        import android.os.Bundle;
        import android.support.v7.app.ActionBarActivity;
        import android.view.LayoutInflater;
        import android.view.Menu;
        import android.view.MenuInflater;
        import android.view.MenuItem;
        import android.view.View;
        import android.view.ViewGroup;
        import android.view.WindowManager;
        import android.view.animation.DecelerateInterpolator;
        import android.widget.AbsListView;
        import android.widget.AdapterView;
        import android.widget.ArrayAdapter;
        import android.widget.ImageView;
        import android.widget.MediaController;
        import android.widget.TextView;
        import android.widget.Toast;
        import android.widget.VideoView;
        import com.etsy.android.grid.StaggeredGridView;
        import com.joanzapata.pdfview.PDFView;

        import org.vudroid.pdfdroid.codec.PdfPage;

        import uk.co.senab.photoview.PhotoViewAttacher;

public class MosaicActivity extends ActionBarActivity implements AbsListView.OnItemClickListener {
    public static  MosaicActivity mosaicActivity;
    private MosaicAdapter mAdapter;
    private static ArrayList<Data> mData;
    private static HashMap<Data,Bitmap> mDataImage;
    private static HashMap<Data, Uri> mDataVideo;
    private static HashMap<Data, File> mDataPdf;
    public static ProgressDialog mosaicProgressDialog;
    public Beacon beacon;
    private Data currentData;
    private android.support.v7.app.ActionBar actionBar;
    private StaggeredGridView staggeredGridView;

    public static boolean isLocked =false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mosaicActivity=this;
        isLocked=false;
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_mosaic);
        beacon = MainActivity.beaconMap.get(getIntent().getStringExtra(MainActivity.MACAdressField));
        actionBar=getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffec6200")));
        actionBar.setTitle(beacon.getName());

        staggeredGridView = (StaggeredGridView) findViewById(R.id.grid_view);
        mosaicProgressDialog =  ProgressDialog.show(this, "Please Wait...", "Loading " + beacon.getName() + " Mosaic..", false, true);
        mosaicProgressDialog.setCancelable(false);
        new Thread(new Runnable() {
            @Override
            public void run()
            {
                if(VideoActivity.videoActivity!=null){
                    VideoActivity.videoActivity.finish();
                    VideoActivity.videoActivity = null;
                }
                if(ImageActivity.imageActivity!=null){
                    ImageActivity.imageActivity.finish();
                    ImageActivity.imageActivity = null;
                }
                if(PdfActivity.pdfActivity!=null){
                    PdfActivity.pdfActivity.finish();
                    PdfActivity.pdfActivity = null;
                }
                mData = generateData();

                mAdapter = new MosaicAdapter(MosaicActivity.this,android.R.layout.simple_list_item_1, mData);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run()
                    {
                        staggeredGridView.setAdapter(mAdapter);
                        staggeredGridView.setOnItemClickListener(MosaicActivity.this);
                        if(mosaicProgressDialog!=null) {
                            mosaicProgressDialog.dismiss();
                            mosaicProgressDialog=null;
                        }
                    }
                });
            }
        }).start();
        // Retrieve and cache the system's default "short" animation time.
    }
    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
    }
    private ArrayList<Data> generateData() {
        ArrayList<Data> listData = new ArrayList<>();
        mDataImage = new HashMap<>();
        mDataVideo = new HashMap<>();
        mDataPdf = new HashMap<>();
        for(Data data : beacon.values()){
            listData.add(data);
        }
        return listData;
    }
    public boolean isLocked(){
        return isLocked;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View thumb1View, int position, long id) {
        staggeredGridView = (StaggeredGridView) findViewById(R.id.grid_view);
        Data data=mData.get(position);
        switch(data.getType()){
            case Data.VIDEO_TYPE :
                if( staggeredGridView.isClickable())
                {
                    staggeredGridView.setClickable(false);
                    staggeredGridView.setLongClickable(false);
                    currentData =data;
                    if(!mDataVideo.containsKey(data)){
                        File file1 = new  File(data.getPath());
                        if(file1.exists())
                        {
                            Uri video = Uri.parse(data.getPath());
                            mDataVideo.put(data,video);
                        }
                    }
                    if(mDataVideo.containsKey(data)){
                        Intent intent = new Intent(MosaicActivity.this, VideoActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.putExtra(MainActivity.videoField, data.getPath());
                        startActivity(intent);

                    }
                }
            break;
            case Data.IMAGE_TYPE :
                if( staggeredGridView.isClickable())
                {
                    staggeredGridView.setClickable(false);
                    staggeredGridView.setLongClickable(false);
                    currentData =data;
                    if(!mDataImage.containsKey(data)){
                        File file = new  File(data.getPath());
                        if(file.exists())
                        {
                            Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                            mDataImage.put(data,bitmap);
                        }
                    }
                    if(mDataImage.containsKey(data)){
                        Intent intent = new Intent(MosaicActivity.this, ImageActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.putExtra(MainActivity.imageField, data.getPath());
                        startActivity(intent);
                    }
                }
            break;
            case Data.PDF_TYPE :
                if( staggeredGridView.isClickable())
                {
                    staggeredGridView.setClickable(false);
                    staggeredGridView.setLongClickable(false);
                    currentData =data;
                    if(!mDataPdf.containsKey(data)){
                        File file = new  File(data.getPath());
                        if(file.exists())
                        {
                            mDataPdf.put(data,file);
                        }
                    }
                    if(mDataPdf.containsKey(data)){
                        Intent intent = new Intent(MosaicActivity.this, PdfActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.putExtra(MainActivity.pdfField, data.getPath());
                        startActivity(intent);
                    }
                }
                break;
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mosaicActivity=null;
        finish();
        isLocked=false;
    }
    @Override
    public void onResume() {
        super.onResume();
        staggeredGridView.setClickable(true);
        staggeredGridView.setLongClickable(true);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_mosaic, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_lock) {
            //onPause();
            if(isLocked){
                isLocked=false;
                if(ScanDevicesService.scanDevicesService!=null)ScanDevicesService.scanDevicesService.onResume();
                item.setIcon(R.drawable.ic_unlock);
                Toast.makeText(this,getText(R.string.action_unlock), Toast.LENGTH_LONG).show();
            }else{
                isLocked=true;
                if(ScanDevicesService.scanDevicesService!=null)ScanDevicesService.scanDevicesService.onPause();
                item.setIcon(R.drawable.ic_lock);
                Toast.makeText(this,getText(R.string.action_lock), Toast.LENGTH_LONG).show();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}