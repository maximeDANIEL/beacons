package com.immersion.beacons;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.RectF;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.etsy.android.grid.util.DynamicHeightImageView;

import org.vudroid.pdfdroid.codec.PdfContext;
import org.vudroid.pdfdroid.codec.PdfDocument;
import org.vudroid.pdfdroid.codec.PdfPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * MosaicAdapter class
 * Created by daniel on 31/03/2015.
 */
public class MosaicAdapter extends ArrayAdapter<Data> {
private final LayoutInflater mLayoutInflater;
private final Random mRandom;
private static final SparseArray<Double> sPositionHeightRatios = new SparseArray<>();
private HashMap<Data,Bitmap> dataMap;

    /**
     * MosaicAdapter constructor
     * @param context
     * @param textViewResourceId
     * @param objects
     */
public MosaicAdapter(Context context, int textViewResourceId,
        ArrayList<Data> objects) {
        super(context, textViewResourceId, objects);
        this.mLayoutInflater = LayoutInflater.from(context);
        this.mRandom = new Random();
        dataMap = new HashMap<>();
        for(Data data : objects){
            File file = new  File(data.getPath());
            if(file.exists()){
                switch(data.getType()){
                    case Data.VIDEO_TYPE :
                        Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(data.getPath(),
                                MediaStore.Images.Thumbnails.MINI_KIND);
                        dataMap.put(data,thumbnail);
                        break;
                    case Data.IMAGE_TYPE :
                        dataMap.put(data,decodeFile(file));
                        break;
                    case Data.PDF_TYPE :
                        dataMap.put(data,decodePdf(file));
                        break;
                }

            }
        }
        }

    /**
     * define mosaic view
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(final int position, View convertView,
                        final ViewGroup parent) {
        ViewHolder vh;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.row_layout_mosaic,
                    parent, false);
            vh = new ViewHolder();
            vh.imgView = (DynamicHeightImageView) convertView
                    .findViewById(R.id.imgView);
            vh.iconView = (ImageView) convertView
                    .findViewById(R.id.iconView);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        double positionHeight = getPositionRatio(position);
        vh.imgView.setHeightRatio(positionHeight);
        Data data = getItem(position);
        Bitmap bitmap = dataMap.get(data);
        if(bitmap!=null){
            vh.imgView.setImageBitmap(bitmap);
            switch(data.getType()){
                case Data.VIDEO_TYPE :
                    vh.iconView.setImageResource(R.drawable.ic_video_file);
                    break;
                case Data.IMAGE_TYPE :
                    vh.iconView.setImageResource(R.drawable.ic_image_file);
                    break;
                case Data.PDF_TYPE :
                    vh.iconView.setImageResource(R.drawable.ic_pdf_file);
                    break;
            }
        }


        return convertView;
    }

    /**
     * get bitmap from pdf
     * @param f
     * @return
     */
    private Bitmap decodePdf(File f){
        PdfContext pdfContext = new PdfContext();
        PdfDocument d = (PdfDocument)pdfContext.openDocument(f.getPath());

        PdfPage vuPage = (PdfPage)d.getPage(0);
        RectF rf = new RectF();
        rf.bottom=rf.right = (float)1.0;
        //,Recttop,left,bottom,right
        //top=0.5,left=0.5,right=1.0,1.0
        return vuPage.renderBitmap(vuPage.getWidth(),vuPage.getHeight(), rf);
    }

    /**
     * get bitmap from file
     * @param f
     * @return
     */
    private Bitmap decodeFile(File f){
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            //The new size we want to scale to
            final int REQUIRED_SIZE=70;

            //Find the correct scale value. It should be the power of 2.
            int scale=1;
            while(o.outWidth/scale/2>=REQUIRED_SIZE && o.outHeight/scale/2>=REQUIRED_SIZE)
                scale*=2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
    static class ViewHolder {
        DynamicHeightImageView imgView;
        ImageView iconView;
    }
    private double getPositionRatio(final int position) {
        double ratio = sPositionHeightRatios.get(position, 0.0);
// if not yet done generate and stash the columns height
// in our real world scenario this will be determined by
// some match based on the known height and width of the image
// and maybe a helpful way to get the column height!
        if (ratio == 0) {
            ratio = getRandomHeightRatio();
            sPositionHeightRatios.append(position, ratio);
        }
        return ratio;
    }
    private double getRandomHeightRatio() {
        return (mRandom.nextDouble() / 2.0) + 1.0; // height will be 1.0 - 1.5
// the width
    }
}
