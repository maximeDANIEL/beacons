package com.immersion.beacons;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.WindowManager;
import com.joanzapata.pdfview.PDFView;
import java.io.File;

/**
 * PdfActivity class
 */
public class PdfActivity extends Activity {
    public static PdfActivity pdfActivity;
    private String path;
    private Integer pageNumber = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pdfActivity=this;
        setContentView(R.layout.activity_pdf);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        path= getIntent().getStringExtra(MainActivity.pdfField);
        PDFView pdfView = (PDFView)findViewById(R.id.pdfView);
        File file = new  File(path);
        if(file.exists())
        {
            pdfView.fromFile(file)
                    .defaultPage(pageNumber)
                    .showMinimap(false)
                    .enableSwipe(true)
                    .load();
        }
    }
}
