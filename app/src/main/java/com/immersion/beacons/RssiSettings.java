package com.immersion.beacons;

/**
 * RssiSettings class
 * Created by daniel on 27/03/2015.
 */
public class RssiSettings {
    private int minRssi;
    private int maxRssi;
    private int sampleSize;

    /**
     * Default constructor
     */
    public RssiSettings(){
    }

    /**
     * RssiSettings constructor with a minRssi, a maxRssi, and a sampleSize
     * @param minRssi
     * @param maxRssi
     * @param sampleSize
     */
    public RssiSettings(int minRssi, int maxRssi,int sampleSize){
        this.minRssi=minRssi;
        this.maxRssi=maxRssi;
        this.sampleSize=sampleSize;
    }
    public int getMinRssi() {
        return minRssi;
    }

    public void setMinRssi(int minRssi) {
        this.minRssi = minRssi;
    }

    public int getMaxRssi() {
        return maxRssi;
    }

    public void setMaxRssi(int maxRssi) {
        this.maxRssi = maxRssi;
    }
    public int getSampleSize() {
        return sampleSize;
    }

    public void setSampleSize(int sampleSize) {
        this.sampleSize = sampleSize;
    }
}
