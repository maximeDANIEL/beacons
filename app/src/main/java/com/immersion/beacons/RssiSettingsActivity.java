package com.immersion.beacons;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * RssiSettingsActivity class
 */
public class RssiSettingsActivity extends ActionBarActivity  implements SeekBar.OnSeekBarChangeListener {
    public static ActionBarActivity actionBarActivity;
    private SeekBar minSeekBar; // declare seekbar object variable
    private SeekBar maxSeekBar; // declare seekbar object variable
    private SeekBar sampleSizeSeekBar; // declare seekbar object variable
    private TextView minTextView; // declare seekbar object variable
    private TextView maxTextView; // declare seekbar object variable
    private TextView sampleSizeTextView; // declare seekbar object variable
    private Button  saveButton;
    private Button  resetButton;
    private android.support.v7.app.ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionBarActivity=this;
        actionBar=getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffec6200")));
        actionBar.setTitle(getText(R.string.rssi_settings));
        setContentView(R.layout.activity_rssi_settings);
        minSeekBar = (SeekBar)findViewById(R.id.rssiMinSeekBar); // make seekbar object
        maxSeekBar = (SeekBar)findViewById(R.id.rssiMaxSeekBar); // make seekbar object
        sampleSizeSeekBar = (SeekBar)findViewById(R.id.sampleSizeSeekBar); // make seekbar object
        minTextView = (TextView)findViewById(R.id.valueRssiMinTextView); // make seekbar object
        maxTextView = (TextView)findViewById(R.id.valueRssiMaxTextView); // make seekbar object
        sampleSizeTextView=(TextView)findViewById(R.id.valueSampleSizeTextView); // make seekbar object
        saveButton = (Button)findViewById(R.id.saveButton);
        resetButton = (Button)findViewById(R.id.resetButton);
        minSeekBar.setOnSeekBarChangeListener(this); // set seekbar listener.
        maxSeekBar.setOnSeekBarChangeListener(this); // set seekbar listener.
        sampleSizeSeekBar.setOnSeekBarChangeListener(this);
        minSeekBar.setProgress(Math.abs(MainActivity.rssiSettings.getMinRssi()));
        maxSeekBar.setProgress(Math.abs(MainActivity.rssiSettings.getMaxRssi()));
        sampleSizeSeekBar.setProgress(Math.abs(MainActivity.rssiSettings.getSampleSize()));
        minTextView.setText(String.valueOf(MainActivity.rssiSettings.getMinRssi()));
        maxTextView.setText(String.valueOf(MainActivity.rssiSettings.getMaxRssi()));
        sampleSizeTextView.setText(String.valueOf(MainActivity.rssiSettings.getSampleSize()));
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              MainActivity.rssiSettings.setMinRssi(-minSeekBar.getProgress());
              MainActivity.rssiSettings.setMaxRssi(-maxSeekBar.getProgress());
              MainActivity.rssiSettings.setSampleSize(sampleSizeSeekBar.getProgress());
              MainActivity.sfm.saveRssiSettings(MainActivity.rssiSettingsFile,MainActivity.rssiSettings);
              Toast.makeText(actionBarActivity,getText(R.string.rssi_save), Toast.LENGTH_LONG).show();
            }
        });
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int minRssi=getResources().getInteger(R.integer.rssi_min_default);
                int maxRssi=getResources().getInteger(R.integer.rssi_max_default);
                int sampleSize=getResources().getInteger(R.integer.sample_size_default);
                MainActivity.rssiSettings.setMinRssi(minRssi);
                MainActivity.rssiSettings.setMaxRssi(maxRssi);
                MainActivity.rssiSettings.setSampleSize(sampleSize);
                minTextView.setText(String.valueOf(minRssi));
                minSeekBar.setProgress(Math.abs(minRssi));
                maxTextView.setText(String.valueOf(maxRssi));
                maxSeekBar.setProgress(Math.abs(maxRssi));
                sampleSizeTextView.setText(String.valueOf(sampleSize));
                sampleSizeSeekBar.setProgress(Math.abs(sampleSize));
                MainActivity.sfm.saveRssiSettings(MainActivity.rssiSettingsFile,MainActivity.rssiSettings);

                Toast.makeText(actionBarActivity,getText(R.string.rssi_reset), Toast.LENGTH_LONG).show();
            }
        });
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffec6200")));
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if(seekBar==minSeekBar) {
            minTextView.setText(String.valueOf(-progress));
            seekBar.setProgress(progress);
            if(maxSeekBar.getProgress()<=progress){
                maxTextView.setText(String.valueOf(-(progress+1)));
                maxSeekBar.setProgress(progress+1);
            }
        }
        if(seekBar==maxSeekBar) {
                maxTextView.setText(String.valueOf(-progress));
                seekBar.setProgress(progress);
            if(minSeekBar.getProgress()>=progress){
                minTextView.setText(String.valueOf(-(progress-1)));
                minSeekBar.setProgress(progress-1);
            }
        }
        if(seekBar==sampleSizeSeekBar) {
            sampleSizeTextView.setText(String.valueOf(progress));
            seekBar.setProgress(progress);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }


}
