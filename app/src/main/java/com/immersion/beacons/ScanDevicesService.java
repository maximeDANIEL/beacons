package com.immersion.beacons;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;

public class ScanDevicesService extends Service {
    public static ScanDevicesService scanDevicesService;
	private BluetoothAdapter mBluetoothAdapter;
	// if no beacon is detected during the idle period, stop scan.
	private static final long IDLE_PERIOD = 500;
	public static String BROADCAST_ACTION = "BT_DEVICES_BROADCAST";
    private BluetoothManager bluetoothManager;
    private Runnable runnable;
    public Handler mHandler = new Handler();
    private boolean isInBackground;
    @Override
	public IBinder onBind(Intent intent) {
		return null;
	}

    /**
     * handle scan result
     */
	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
		public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            if(MainActivity.beaconMap.containsKey(device.getAddress())) {
                Intent intent = new Intent(BROADCAST_ACTION);
                BluetoothDeviceData data = new BluetoothDeviceData(device.getAddress(), rssi);
                intent.putExtra(MainActivity.MACAdressField, data);
                sendBroadcast(intent);
            }
		};
	};
    /**
     * when creating scan
     */
	public void onCreate() {
		super.onCreate();
        scanDevicesService=this;
		registerReceiver(new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				onPause();
			}
		}, new IntentFilter("com.example.pause"));

		registerReceiver(new BroadcastReceiver() {


            @Override
            public void onReceive(Context arg0, Intent arg1) {
                onResume();

            }
        }, new IntentFilter("com.example.resume"));
        isInBackground=false;
		bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = bluetoothManager.getAdapter();
		scanLeDevice(mBluetoothAdapter.isEnabled());
	}
    /**
     * when destroying scan
     */
	@Override
	public void onDestroy() {
		super.onDestroy();

        mBluetoothAdapter.stopLeScan(mLeScanCallback);
            if( mHandler!=null){
               mHandler.removeCallbacks(ScanDevicesService.scanDevicesService.runnable);
            }
		//Destroying Service
	}

    /**
     * when pausing scan
     */
	void onPause() {
		isInBackground = true;
        mBluetoothAdapter.stopLeScan(mLeScanCallback);

	}
    /**
     * when resuming scan
     */
	void onResume() {
		isInBackground = false;
        mBluetoothAdapter.stopLeScan(mLeScanCallback);
		scanLeDevice(true);
	}

    /**
     * when finishing scan
     */
    void finish() {
        isInBackground = false;
    }

    /**
     * scanning loop
     * @param enable
     */
	private void scanLeDevice(final boolean enable) {
        if (enable && MainActivity.isSearchingForBeacons && !MosaicActivity.isLocked) {
            // Stops scanning after a pre-defined scan period.
            if (!isInBackground) {
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        mBluetoothAdapter.stopLeScan(mLeScanCallback);
                        Intent intent = new Intent(BROADCAST_ACTION);
                        sendBroadcast(intent);
                        scanLeDevice(true);

                    }
                };
                mHandler = new Handler();
                mHandler.postDelayed(runnable, IDLE_PERIOD);
            }
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        }
    }
}

