package com.immersion.beacons;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

/**
 * SettingActivity class
 */
public class SettingActivity extends ActionBarActivity implements AdapterView.OnItemClickListener{
    public static SettingActivity settingActivity;
    private ArrayAdapter<Beacon> mAdapter;
    private ListView beaconListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        settingActivity=this;
        setContentView(R.layout.activity_setting);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffec6200")));
        getSupportActionBar().setTitle(getText(R.string.action_settings));
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        beaconListView = (ListView) findViewById(R.id.beaconList);

        final List<Beacon> beaconsList = new ArrayList<>();
        beaconsList.addAll(MainActivity.beaconMap.values());
        mAdapter= new ArrayAdapter<Beacon>(this,R.layout.row_layout,beaconsList){
            @Override
        public View getView(int position, View convertView, ViewGroup parent){
                if(convertView==null){
                    LayoutInflater inflater= (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.row_layout,parent,false);
                }

                TextView text1 = (TextView) convertView.findViewById(R.id.textView1);
                TextView text2 = (TextView) convertView.findViewById(R.id.textView2);
                text1.setText(beaconsList.get(position).getName());
                text2.setText(beaconsList.get(position).getMACAdress());
                return convertView;
            }
        };
        beaconListView.setAdapter(mAdapter);
        beaconListView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String selectedBeaconMacAdress =  mAdapter.getItem(position).getMACAdress();
        Intent intent = new Intent(SettingActivity.this, ContentSettingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(MainActivity.MACAdressField,selectedBeaconMacAdress);
        startActivity(intent);
    }
}
