package com.immersion.beacons;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

/**
 * SettingsFileManager class
 * Created by daniel on 27/03/2015.
 */
public class SettingsFileManager {
    public static final String minRssiField ="minRssi:";
    public static final String maxRssiField ="maxRssi:";
    public static final String sampleSizeField ="sampleSize:";
    private Context context;
    public SettingsFileManager(Context context){
        this.setContext(context);
    }
    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
    public void saveRssiSettings(String filename, RssiSettings rssiSettings){
        FileOutputStream output;
        try {
            output = context.openFileOutput(filename, Context.MODE_PRIVATE);
            String minRssi = minRssiField+"\n"+rssiSettings.getMinRssi()+"\n";
            String maxRssi = maxRssiField+"\n"+rssiSettings.getMaxRssi()+"\n";
            String sampleSize = sampleSizeField+"\n"+rssiSettings.getSampleSize()+"\n";
            try {
                output.write(minRssi.getBytes());
                output.write(maxRssi.getBytes());
                output.write(sampleSize.getBytes());
            } catch (IOException f) {
                f.printStackTrace();
            }
            output.close();
        } catch (FileNotFoundException f) {
            f.printStackTrace();
        } catch (IOException h) {
            h.printStackTrace();
        }
    }
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public RssiSettings loadRssiSettings(String filename) {
        RssiSettings res = new RssiSettings();
        try(FileInputStream input = context.openFileInput(filename)) {
            Scanner sc = new Scanner(input);
            String line;
            while (sc.hasNextLine()) {
                line = sc.nextLine();
                if(line.equals(minRssiField)){
                    line = sc.nextLine();
                    res.setMinRssi(Integer.parseInt(line));
                }
                if(line.equals(maxRssiField)){
                    line = sc.nextLine();
                    res.setMaxRssi(Integer.parseInt(line));
                }
                if(line.equals(sampleSizeField)){
                    line = sc.nextLine();
                    res.setSampleSize(Integer.parseInt(line));
                }
            }
            input.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return res;
    }
}