package com.immersion.beacons;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;
import java.io.File;

/**
 * VideoActivity class
 */
public class VideoActivity extends Activity {
    public static VideoActivity videoActivity=null;
    private String path;
    private MediaController mediaController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        videoActivity=this;
        setContentView(R.layout.activity_video);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mediaController = new MediaController(this);
        path= getIntent().getStringExtra(MainActivity.videoField);
        VideoView videoView = (VideoView)findViewById(R.id.videoView);
        File file = new  File(path);
        if(file.exists())
        {
            Uri video = Uri.parse(path);
            mediaController.setAnchorView(videoView);
            videoView.setVideoURI(video);
            videoView.setMediaController(mediaController);
            videoView.start();
        }
    }
}
